package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CoberturaPage extends BasePage {

    private By menuPrincipal = By.cssSelector("body > app-root > ma-navbar > nav > ul > li.nav-item.burguer-menu > ma-burguer-menu > button > img");
    private By menuCobertura = By.cssSelector("body > app-root > ma-navbar > nav > ul > li.nav-item.burguer-menu > ma-burguer-menu > nav > ul > li:nth-child(3) > button > span");
    private By crearCoberturaButton = By.cssSelector("body > app-root > ma-coberturas-abm > ma-page-template > div > div.section-container > div > div.buttons-wrapper > ma-button:nth-child(2) > button > span");
    private By codigoInput = By.cssSelector("body > app-root > ma-crear-cobertura > ma-page-template > div > div.section-container > div > div:nth-child(2) > ma-input > div > div > input");
    private By descripcionInput = By.cssSelector("body > app-root > ma-crear-cobertura > ma-page-template > div > div.section-container > div > div:nth-child(4) > ma-input > div > div > input");
    private By tipoCoberturaSelect = By.cssSelector("body > app-root > ma-crear-cobertura > ma-page-template > div > div.section-container > div > div:nth-child(6) > ma-select > div > div > select");
    private By ramoSelect = By.cssSelector("body > app-root > ma-crear-cobertura > ma-page-template > div > div.section-container > div > div:nth-child(3) > ma-select > div > div > select");
    private By estadoSelect = By.cssSelector("body > app-root > ma-crear-cobertura > ma-page-template > div > div.section-container > div > div:nth-child(5) > ma-select > div > div > select");
    private By crearCoberturaButton2 = By.cssSelector("body > app-root > ma-crear-cobertura > ma-page-template > div > div.section-container > div > div.box.column-all.justify-self-end > ma-button:nth-child(2) > button > span");

    public CoberturaPage(WebDriver webDriver){

        super(webDriver);
    }

    public void ClickOnMenuPrincipal(){

        webDriver.findElement(menuPrincipal).click();
    }

    public void ClickOnMenuCobertura(){

        webDriver.findElement(menuCobertura).click();
    }

    public void ClickOnButtonCrearCobertura() throws InterruptedException {
        Thread.sleep(1000);
        webDriver.findElement(crearCoberturaButton).click();
    }

    public void writeCodigo(String codigo){

        webDriver.findElement(codigoInput).sendKeys(codigo);
    }

    public void writeDescripcion(String descripcion){

        webDriver.findElement(descripcionInput).sendKeys(descripcion);
    }

    public void selectTipoCobertura(String cobertura){
        Select select = new Select(webDriver.findElement(tipoCoberturaSelect));
        select.selectByVisibleText(cobertura);

    }
    public void selectRamo(String ramo){
        Select select = new Select(webDriver.findElement(ramoSelect));
        select.selectByVisibleText(ramo);

    }

    public void selectEstado(String estado){
        Select select = new Select(webDriver.findElement(estadoSelect));
        select.selectByVisibleText(estado);

    }

    public void clickOnCrearCobertura(){
        webDriver.findElement(crearCoberturaButton2).click();

    }
}
