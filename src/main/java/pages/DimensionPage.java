package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DimensionPage extends BasePage{
    private By menuPrincipal = By.cssSelector("body > app-root > ma-navbar > nav > ul > li.nav-item.burguer-menu > ma-burguer-menu > button > img");
    private By menuDimension = By.cssSelector("body > app-root > ma-navbar > nav > ul > li.nav-item.burguer-menu > ma-burguer-menu > nav > ul > li:nth-child(4) > button > span");
    private By crearDimensionButton = By.cssSelector("body > app-root > ma-dimensiones-abm > ma-page-template > div > div.section-container > div > div.buttons-wrapper > ma-button:nth-child(2) > button > span");
    private By codigoDimensionInput = By.cssSelector("body > app-root > ma-crear-dimension > ma-page-template > div > div.section-container > div > form > div > ma-input:nth-child(1) > div > div > input");
    private By descripcionDimensionInput = By.cssSelector("body > app-root > ma-crear-dimension > ma-page-template > div > div.section-container > div > form > div > ma-input:nth-child(2) > div > div > input");
    private By estadoDimensionSelect = By.cssSelector("body > app-root > ma-crear-dimension > ma-page-template > div > div.section-container > div > form > div > div:nth-child(3) > ma-select > div > div > select");
    private By atributoDimensionSelect = By.cssSelector("body > app-root > ma-crear-dimension > ma-page-template > div > div.section-container > div > form > div > div:nth-child(4) > ma-select > div > div > select");
    private By crearDimensionButtonDim = By.cssSelector("body > app-root > ma-crear-dimension > ma-page-template > div > div.section-container > div > div.form-submit > ma-button:nth-child(2) > button > span");
    private By modalMensaje = By.cssSelector("#custom-modal-1 > div.modal > div > span");

    public DimensionPage(WebDriver webDriver){

        super(webDriver);
    }

    public void ClickOnMenuPrincipal(){

        webDriver.findElement(menuPrincipal).click();
    }

    public void ClickOnMenuDimension(){

        webDriver.findElement(menuDimension).click();
    }


    public void ClickOnButtonCrearDimension() throws InterruptedException {
        Thread.sleep(5000);
        webDriver.findElement(crearDimensionButton).click();
    }

    public void writeCodigoDimension(String codigo) throws InterruptedException {
        Thread.sleep(5000);
        webDriver.findElement(codigoDimensionInput).sendKeys(codigo);
    }

    public void writeDescripcionDimension(String descripcion) throws InterruptedException {
        Thread.sleep(5000);
        webDriver.findElement(descripcionDimensionInput).sendKeys(descripcion);
    }

    public void selectEstadoDimension(String estado) throws InterruptedException {
        Thread.sleep(5000);

        Select select = new Select(webDriver.findElement(estadoDimensionSelect));
        select.selectByVisibleText(estado);

    }

    public void selectAtributoDimension(String atributo) throws InterruptedException {
        Thread.sleep(5000);
        Select select = new Select(webDriver.findElement(atributoDimensionSelect));
        select.selectByVisibleText(atributo);
    }

    public void clickOnCrearDimension() throws InterruptedException {
        Thread.sleep(5000);
        webDriver.findElement(crearDimensionButtonDim).click();

    }

    //public void addDimension(String codigo, String descripcion, String estado, String atributo) throws InterruptedException {
        //writeCodigoDimension(codigo);
        //writeDescripcionDimension(descripcion);
        //selectEstadoDimension(estado);
        //selectAtributoDimension(atributo);
        //clickOnCrearDimension();
    //}

    public boolean isAlertVisible(){
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 3);
            wait.until(ExpectedConditions.visibilityOf(webDriver.findElement(modalMensaje)));
            return true;
        }catch (Exception e){
            return false;
        }
    }

}
