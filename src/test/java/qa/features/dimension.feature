Feature: Administrar Dimension

  Background: Ir al menu Dimension
    Given actuario quiere ir a Dimension

  Scenario: Crear dimension con datos requeridos
    When actuario quiere crear dimension con datos requeridos
    Then actuario tiene dimension creada con datos requeridos

  Scenario: Crear dimension sin datos requeridos
    When actuario quiere crear dimension sin datos requeridos
    Then actuario no tiene dimension creada sin datos requeridos

  Scenario: Crear dimension con codigo existente
    When actuario quiere crear dimension con codigo existente
    Then actuario no tiene dimension creada con codigo existente

  Scenario: Editar dimension existente
    When actuario quiere editar dimension existente
    Then actuario tiene dimension editada

  Scenario: Eliminar dimension existente aceptar
    When actuario quiere eliminar dimension aceptar
    Then actuario tiene dimension eliminada

  Scenario: Eliminar dimension existente aceptar asociada
    When actuario quiere eliminar dimension aceptar asociada
    Then actuario no tiene dimension eliminada asociada

  Scenario: Listar ascendente dimension existente
    When actuario quiere ordenar ascendente dimension existente
    Then actuario tiene ordenado ascendente dimension existente

  Scenario: Listar descendente dimension existente
    When actuario quiere ordenar descendente dimension existente
    Then actuario tiene ordenado descendente dimension existente
