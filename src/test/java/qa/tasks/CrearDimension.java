package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.CrearDimensionServices;
import qa.pageobjects.HomeDimensionServices;

@Component
public class CrearDimension {

    @Autowired
    private HomeDimensionServices homeDimensionServices;

    @Autowired
    private CrearDimensionServices crearDimensionServices;

    public void withInfoRequired(){
        homeDimensionServices.clickOnButtonCrear();
        crearDimensionServices.writeInputCrearCodigo("001");
        crearDimensionServices.selectEstado("Activo");
        crearDimensionServices.writeInputCrearDescripcion("DIMTEST0001");
        crearDimensionServices.selectAtributo("EDAD");
        crearDimensionServices.clickOnButtonConfirmar();
    }

    public void withOutInfoRequired(){
        homeDimensionServices.clickOnButtonCrear();
        crearDimensionServices.writeInputCrearCodigo("");
        crearDimensionServices.selectEstado("Seleccione una opción");
        crearDimensionServices.writeInputCrearDescripcion("");
        crearDimensionServices.selectAtributo("Seleccione una opción");
        crearDimensionServices.clickOnButtonConfirmar();
    }

    public void withCodigoExistente(){
        homeDimensionServices.clickOnButtonCrear();
        crearDimensionServices.writeInputCrearCodigo("DIMTEST0001");
        crearDimensionServices.selectEstado("Activo");
        crearDimensionServices.writeInputCrearDescripcion("DIMTEST 0001");
        crearDimensionServices.selectAtributo("EDAD");
        crearDimensionServices.clickOnButtonConfirmar();
    }

}
