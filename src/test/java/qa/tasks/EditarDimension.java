package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EditarDimensionServices;
import qa.pageobjects.HomeDimensionServices;

@Component
public class EditarDimension {

    @Autowired
    private HomeDimensionServices homeDimensionServices;

    @Autowired
    private EditarDimensionServices editarDimensionServices;

    public void editarWithInfoRequired() throws InterruptedException {
        homeDimensionServices.clickOnTablaMostrarAccion();
        homeDimensionServices.clickOnTablaButtonEditar();
        Thread.sleep(1000);
        editarDimensionServices.selectEditarEstado("Inactivo");
        editarDimensionServices.writeEditarDescripcion("DIMTEST0002");
        editarDimensionServices.clickOnButtonConfirmar();
    }

}
