package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EliminarDimensionServices;
import qa.pageobjects.HomeDimensionServices;

@Component
public class EliminarDimension {

    @Autowired
    private HomeDimensionServices homeDimensionServices;

    @Autowired
    private EliminarDimensionServices eliminarDimensionServices;

     public void eliminarAceptarDimensionAsociada() {
        homeDimensionServices.clickOnTablaMostrarAccion();
        homeDimensionServices.clickOnTablaButtonEliminar();
         eliminarDimensionServices.clickOnTablaButtonBorrar();

        }

    public void eliminarAceptarDimension() {
        homeDimensionServices.clickOnTablaMostrarAccion();
        homeDimensionServices.clickOnTablaButtonEliminar();
        eliminarDimensionServices.clickOnTablaButtonBorrar();

    }

}
