package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.HomeDimensionServices;

@Component
public class MenuDimension {
    @Autowired
    private HomeDimensionServices homeDimensionServices;

    public void irMenuVersionado(){
        homeDimensionServices.clickOnMenuPrincipal();
        homeDimensionServices.clickOnMenuDimension();
    }
}
