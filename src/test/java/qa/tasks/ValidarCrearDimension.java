package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.CrearDimensionServices;

@Component
public class ValidarCrearDimension {

    @Autowired
    private CrearDimensionServices crearDimensionServices;

    public boolean validarDimensionWithInfoDefault(){
        String mensaje = crearDimensionServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Se creó dimension con éxito");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarDimensionWithOutInfoDefault(){
        String mensaje = crearDimensionServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Faltan campos");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarDimensionWithCodigoExistente(){
        String mensaje = crearDimensionServices.getMensajeCrear();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("a se encuentra una dimension con el codigo");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;

    }

}
