package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EditarDimensionServices;

@Component
public class ValidarEditarDimension {

    @Autowired
    private EditarDimensionServices editarDimensionServices;

    public boolean validarDimensionEditarWithInfoRequired(){
        String mensaje = editarDimensionServices.getMensajeEditar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Se actualizó dimension");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }




}
