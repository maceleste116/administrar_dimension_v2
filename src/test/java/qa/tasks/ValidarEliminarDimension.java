package qa.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qa.pageobjects.EliminarDimensionServices;

@Component
public class ValidarEliminarDimension {

    @Autowired
    private EliminarDimensionServices eliminarDimensionServices;

    public boolean validarDimensionEliminarAceptar(){
        String mensaje = eliminarDimensionServices.getMensajeBorrar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Se eliminó");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }

    public boolean validarEliminarAceptarDimensionAsociada(){
        String mensaje = eliminarDimensionServices.getMensajeBorrar();
        System.out.println("El mensaje obtenido es:" + mensaje);
        boolean contieneMensaje = mensaje.contains("Falló al eliminar dimension");
        System.out.println("Contiene el mensaje?" + contieneMensaje);
        return contieneMensaje;
    }



}
