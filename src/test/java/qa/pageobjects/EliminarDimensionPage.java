package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class EliminarDimensionPage extends PageBase {

    @Autowired
    public EliminarDimensionPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="dimensiones-modal-button-confimar")
    private WebElement ButtonBorrar;

    @FindBy(id="dimensiones-modal-button-cancelar")
    private WebElement ButtonCancelar;

    @FindBy(id="dimensiones-mensaje")
    private WebElement mensajeBorrar;


}
