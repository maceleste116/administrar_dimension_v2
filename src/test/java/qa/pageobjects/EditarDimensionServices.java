package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EditarDimensionServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private EditarDimensionPage editarDimensionPage;

    public void selectEditarEstado(String estado){
        new Select(this.editarDimensionPage.getSelectEditarEstado()).selectByVisibleText(estado);
    }

    public void writeEditarDescripcion(String descripcion){
        this.editarDimensionPage.getInputEditarDescripcion().sendKeys(descripcion);
    }


    public void clickOnButtonCancelar() {
        this.editarDimensionPage.getButtonCancelar().click();
    }

    public void clickOnButtonConfirmar() {
        this.editarDimensionPage.getButtonConfirmar().click();
    }

    public String getMensajeEditar(){
        this.wait.until(ExpectedConditions.visibilityOf(this.editarDimensionPage.getMensajeEditar()));
        return this.editarDimensionPage.getMensajeEditar().getText();
    }


}
