package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CrearDimensionPage extends PageBase {

    @Autowired
    public CrearDimensionPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="crear-dimension-input-codigo")
    private WebElement inputCrearCodigo;

    @FindBy(id="crear-dimension-select-estado")
    private WebElement selectEstado;

    @FindBy(id="crear-dimension-input-descripcion")
    private WebElement inputCrearDescripcion;

    @FindBy(id="crear-dimension-select-codigoAtributo")
    private WebElement selectAtributo;

    @FindBy(id="crear-dimension-boton-Cancelar")
    private WebElement buttonCancelar;

    @FindBy(id="crear-dimension-boton-Confirmar")
    private WebElement buttonConfirmar;

    @FindBy(id="crear-dimension-modal-mensaje")
    private WebElement mensajeCrear;


}
