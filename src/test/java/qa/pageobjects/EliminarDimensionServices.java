package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EliminarDimensionServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private EliminarDimensionPage eliminarDimensionPage;

    @Autowired
    private HomeDimensionPage homeDimensionPage;

    public void clickOnTablaButtonBorrar() {
        this.wait.until(ExpectedConditions.visibilityOf(eliminarDimensionPage.getButtonBorrar()));
        this.eliminarDimensionPage.getButtonBorrar().click();
    }

    public void clickOnTablaButtonCancelar() {
        this.wait.until(ExpectedConditions.visibilityOf(eliminarDimensionPage.getButtonBorrar()));
        this.eliminarDimensionPage.getButtonBorrar().click();
    }

    public String getMensajeBorrar(){
        this.wait.until(ExpectedConditions.visibilityOf(this.eliminarDimensionPage.getMensajeBorrar()));
        return this.eliminarDimensionPage.getMensajeBorrar().getText();
    }


}
