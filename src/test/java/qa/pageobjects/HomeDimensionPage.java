package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class HomeDimensionPage extends PageBase {

    @Autowired
    public HomeDimensionPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="nabvar-burguer-menu-boton-desplegar")
    private WebElement menuPrincipal;

    @FindBy(id="nabvar-burguer-menu-boton-dimensiones")
    private WebElement menuDimension;

    @FindBy(id="dimensiones-tabla-boton-mostrar-acciones-0")
    private WebElement tablaMostrarAccion;

    @FindBy(id="dimensiones-boton-crearDimension")
    private WebElement buttonCrear;

    @FindBy(id="dimensiones-boton-volver")
    private WebElement buttonVolver;

    @FindBy(id="dimensiones-tabla-boton-editar-0")
    private WebElement tablaButtonEditar;

    @FindBy(id="dimensiones-tabla-boton-eliminar-0")
    private WebElement tablaButtonEliminar;

    //TABLA

    @FindBy(id="dimensiones-tabla")
    private WebElement mostrarTableHome;

    @FindBy(id="dimensiones-tabla-icono-orden-codigo")
    private WebElement iconSortAsc;

    @FindBy(id="dimensiones-tabla-icono-orden-codigo")
    private WebElement iconSortDesc;


}
