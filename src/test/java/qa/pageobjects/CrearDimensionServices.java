package qa.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrearDimensionServices {

    @Autowired
    private WebDriver driver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private CrearDimensionPage crearDimensionPage;

    public void writeInputCrearCodigo(String codigo){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearDimensionPage.getInputCrearCodigo()));
        this.crearDimensionPage.getInputCrearCodigo().sendKeys(codigo);
    }

    public void selectEstado(String estado){
        new Select(this.crearDimensionPage.getSelectEstado()).selectByVisibleText(estado);
    }

    public void writeInputCrearDescripcion(String descripcion){
        this.crearDimensionPage.getInputCrearDescripcion().sendKeys(descripcion);
    }

    public void selectAtributo(String atributo){
        new Select(this.crearDimensionPage.getSelectAtributo()).selectByVisibleText(atributo);
    }

    public void clickOnButtonCancelar() {
        this.crearDimensionPage.getButtonCancelar().click();
    }

    public void clickOnButtonConfirmar() {
        this.crearDimensionPage.getButtonConfirmar().click();
    }

    public String getMensajeCrear(){
        this.wait.until(ExpectedConditions.visibilityOf(this.crearDimensionPage.getMensajeCrear()));
        return this.crearDimensionPage.getMensajeCrear().getText();
    }


}
