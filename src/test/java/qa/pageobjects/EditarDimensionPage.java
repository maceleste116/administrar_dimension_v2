package qa.pageobjects;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class EditarDimensionPage extends PageBase {

    @Autowired
    public EditarDimensionPage(WebDriver driver){
        super(driver);
    }

    @FindBy(id="editar-dimension-input-codigo")
    private WebElement inputEditarCodigo;

    @FindBy(id="editar-dimension-select-estado")
    private WebElement selectEditarEstado;

    @FindBy(id="editar-dimension-input-descripcion")
    private WebElement inputEditarDescripcion;

    @FindBy(id="editar-dimension-select-codigoAtributo")
    private WebElement selectEditarAtributo;

    @FindBy(id="editar-dimension-boton-Cancelar")
    private WebElement buttonCancelar;

    @FindBy(id="editar-dimension-boton-Confirmar")
    private WebElement buttonConfirmar;

    @FindBy(id="editar-dimension-modal-mensaje")
    private WebElement mensajeEditar;

}
