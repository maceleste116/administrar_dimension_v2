package qa.stepdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.PendingException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import qa.conf.DriverConfig;
import qa.tasks.*;

@CucumberContextConfiguration
@ContextConfiguration(classes = {DriverConfig.class})
public class DimensionStepDefs{

    @Autowired
    private NavigateTo navigateTo;

    @Autowired
    private MenuDimension menuDimension;

    @Autowired
    private CrearDimension crearDimension;

    @Autowired
    private ValidarCrearDimension validarCrearDimension;

    @Autowired
    private EditarDimension editarDimension;

    @Autowired
    private ValidarEditarDimension validarEditarDimension;

    @Autowired
    private EliminarDimension eliminarDimension;

    @Autowired
    private ValidarEliminarDimension validarEliminarDimension;

    @Before
    public void goHomePage() throws InterruptedException {
        navigateTo.homePage();
        Thread.sleep(1000);
    }

    @Given("^actuario quiere ir a Dimension$")
    public void actuario_quiere_ir_a_dimension() throws Throwable {
        menuDimension.irMenuVersionado();
        Thread.sleep(1000);
    }

    @When("^actuario quiere crear dimension con datos requeridos$")
    public void actuario_quiere_crear_dimension_con_datos_requeridos() throws Throwable {
        crearDimension.withInfoRequired();
        Thread.sleep(1000);
    }

    @Then("^actuario tiene dimension creada con datos requeridos$")
    public void actuario_tiene_dimension_creada_con_datos_requeridos() throws Throwable {
        Assert.assertTrue(validarCrearDimension.validarDimensionWithInfoDefault());
    }

    @When("^actuario quiere crear dimension sin datos requeridos$")
    public void actuario_quiere_crear_dimension_sin_datos_requeridos() throws Throwable {
        Thread.sleep(1000);
        crearDimension.withOutInfoRequired();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene dimension creada sin datos requeridos$")
    public void actuario_no_tiene_dimension_creada_sin_datos_requeridos() throws Throwable {
        Assert.assertTrue(validarCrearDimension.validarDimensionWithOutInfoDefault());
    }

    @When("^actuario quiere crear dimension con codigo existente$")
    public void actuario_quiere_crear_dimension_con_codigo_existente() throws Throwable {
        Thread.sleep(1000);
        crearDimension.withCodigoExistente();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene dimension creada con codigo existente$")
    public void actuario_no_tiene_dimension_creada_con_codigo_existente() throws Throwable {
        Assert.assertTrue(validarCrearDimension.validarDimensionWithCodigoExistente());
    }

    @When("^actuario quiere editar dimension existente$")
    public void actuario_quiere_editar_dimension_existente() throws Throwable {
        Thread.sleep(1000);
        editarDimension.editarWithInfoRequired();
        Thread.sleep(1000);
    }

    @Then("^actuario tiene dimension editada$")
    public void actuario_tiene_dimension_editada() throws Throwable {
        Assert.assertTrue(validarEditarDimension.validarDimensionEditarWithInfoRequired());
    }

    @When("^actuario quiere eliminar dimension aceptar$")
    public void actuario_quiere_eliminar_dimension_aceptar() throws Throwable {
        Thread.sleep(1000);
        eliminarDimension.eliminarAceptarDimension();
        Thread.sleep(1000);
    }

    @Then("^actuario tiene dimension eliminada$")
    public void actuario_tiene_dimension_eliminada() throws Throwable {
        Assert.assertTrue(validarEliminarDimension.validarDimensionEliminarAceptar());

    }

    @When("^actuario quiere eliminar dimension aceptar asociada$")
    public void actuario_quiere_eliminar_dimension_aceptar_asociada() throws Throwable {
        Thread.sleep(1000);
        eliminarDimension.eliminarAceptarDimensionAsociada();
        Thread.sleep(1000);
    }

    @Then("^actuario no tiene dimension eliminada asociada$")
    public void actuario_no_tiene_dimension_eliminada_asociada() throws Throwable {
        Assert.assertTrue(validarEliminarDimension.validarEliminarAceptarDimensionAsociada());
    }

    @When("^actuario quiere ordenar ascendente dimension existente$")
    public void actuario_quiere_ordenar_ascendente_dimension_existente() throws Throwable {
        throw new PendingException();
    }

    @Then("^actuario tiene ordenado ascendente dimension existente$")
    public void actuario_tiene_ordenado_ascendente_dimension_existente() throws Throwable {
        throw new PendingException();
    }

    @When("^actuario quiere ordenar descendente dimension existente$")
    public void actuario_quiere_ordenar_descendente_dimension_existente() throws Throwable {
        throw new PendingException();
    }

    @Then("^actuario tiene ordenado descendente dimension existente$")
    public void actuario_tiene_ordenado_descendente_dimension_existente() throws Throwable {
        throw new PendingException();
    }


}




